﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using HtmlAgilityPack;

namespace http_client_scraping
{
    class Program
    {
        static void Main(string[] args)
        {
            /*using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://login.yahoo.com/?.intl=us&.lang=en-US&.src=finance&authMechanism=primary&done=https%3A%2F%2Ffinance.yahoo.com%2F&eid=100&add=1");
                client.Timeout = TimeSpan.FromSeconds(20);
                //var page = client.GetStringAsync("");

                var content = new FormUrlEncodedContent(new []
                {
                    new KeyValuePair<string, string>("", "login") 
                });

                var login = client.PostAsync("", content);
                var resultContent = login.Result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);

            }*/

            var http = WebRequest.Create(
                    "https://login.yahoo.com/?.intl=us&.lang=en-US&.src=finance&authMechanism=primary&done=https%3A%2F%2Ffinance.yahoo.com%2F&eid=100&add=1")
                as HttpWebRequest;
            http.KeepAlive = true;
            http.Method = "GET";

            //get response as type WebResponse, so we can grab header/cookie information
            var response = http.GetResponse();

            //convert response to Stream, for parsing HTML to use X-Path
            var data = response.GetResponseStream();

            //Y!F sets the below 3 values in the login page, which must be passed in to the POST login request
            string crumb;
            string aCrumb;
            string sessionIndex;
            
            //grab the above 3 values using X-Path
            using (System.IO.Stream httpResponse = data)
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(httpResponse))
                {
                    var responseData = sr.ReadToEnd();
                    var htmlDoc = new HtmlDocument();
                    htmlDoc.LoadHtml(responseData);

                    crumb = htmlDoc.DocumentNode.SelectSingleNode("//body//div[2]//input[@name='crumb']").GetAttributeValue("value", null);
                    aCrumb = htmlDoc.DocumentNode.SelectSingleNode("//body//div[2]//input[@name='acrumb']").GetAttributeValue("value", null);
                    sessionIndex = htmlDoc.DocumentNode.SelectSingleNode("//body//div[2]//input[@name='sessionIndex']").GetAttributeValue("value", null);

                    
                }
            }

            Console.WriteLine(crumb);
            Console.WriteLine(aCrumb);
            Console.WriteLine(sessionIndex);

            var headers = response.Headers;
            string[] headerKeys = headers.AllKeys;

            foreach (var item in headerKeys)
            {
                Console.WriteLine(item);
            }

                //var data = httpResponse.GetResponseStream();


                http = WebRequest.Create("https://login.yahoo.com/account/challenge/password?.src=fpctx&.intl=us&.lang=en-US&authMechanism=primary&done=https%3A%2F%2Fwww.yahoo.com%2F&add=1&display=login&yid=sts1789&sessionIndex=Qg--&acrumb=p9UXaAMF") as HttpWebRequest;

            http.CookieContainer = new CookieContainer();
            //http.CookieContainer.Add(httpResponse.Cookies);



            //http.ContentType = "application/x-www-form-urlencoded";
            //http.B .Headers .Add("Form Data");
        }
    }
}
